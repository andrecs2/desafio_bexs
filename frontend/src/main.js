import './assets/css/main.css';

import Vue from 'vue'

import App from './App.vue'
import store  from './config/store'
import router from './config/router'
import './config/bootstrap'
import './config/moment'
import './config/axios'

window.axios = require('axios');
axios.defaults.baseURL = 'http://localhost:9090/'

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})

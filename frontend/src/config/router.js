import Vue from 'vue'
import Router from 'vue-router'
import AnswerCard from '../components/answer/AnswerCard.vue'
import QuestionCard from '../components/question/QuestionCard.vue'

Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: QuestionCard
    },
    {
      path: '/question',
      name: 'question',
      component: AnswerCard
    }
  ]
})
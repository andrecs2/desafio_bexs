import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        perguntas: []
        ,item: {}
    },
    getters: {
        appVersion: (state) => {
          return state.packageVersion
        },
        perguntas: state => state.perguntas,
        item: state => state.item
    }
    ,mutations:{
        adicionarPergunta(state, pergunta){
            this.state.perguntas.push({
                id: this.state.perguntas.length +1,
                text: pergunta.text,
                user: pergunta.user,
                respostas: [],
                likes: 0,
                creationDate: new Date(),
                replys: 0
            })
          },
        adicionarResposta(state, resposta){
          resposta.id = this.state.perguntas.find(obj => obj.id == resposta.pergunta.id).respostas.length +1
          resposta.creationDate = new Date()
          this.state.perguntas.find(obj => obj.id == resposta.pergunta.id).respostas.push(
            resposta
          )
          this.state.perguntas.find(obj => obj.id == resposta.pergunta.id).replys++;
            
          },
          likePergunta(state, id){
            this.state.perguntas.find(obj => obj.id == id).likes++
          },
          likeResposta(state, item){
            this.state.perguntas.find(obj => obj.id == item.pergunta.id).respostas.find(obj => obj.id == item.id).likes++
          },
    }
})
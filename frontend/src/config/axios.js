import ax from 'axios'

export const axios = ax

export default() => {
    return axios.create({
        baseURL: `http://localhost:9000/`,
        withCredentials: false,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
}
package br.com.bexs.desafio_bexs.enums;

public enum AnsweredEnum {
	ALL("A")
	,NOT("N")
	,YES("Y");
	
	private String value;

	AnsweredEnum(String value){
		this.value = value;
	}

	public static AnsweredEnum get(String value) {
		for (AnsweredEnum o : AnsweredEnum.values()) {
			if (o.value.equals(value)) {
				return o;
			}
		}
		return null;
	}

}

package br.com.bexs.desafio_bexs.model;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicInsert
@DynamicUpdate
public class Question {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String text;

	@Column
	private String user;
	
	@Column
	private long likes;

	@Column
	private long replys;
	
	@Column
	private Date creationDate;
	
	@OneToMany(mappedBy = "question")
	private List<Answer> answers;
	
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setLikes(long likes) {
		this.likes = likes;
	}

	public void setReplys(long replys) {
		this.replys = replys;
	}

	public Long getLikes() {
		return likes;
	}

	public Long getReplys() {
		return replys;
	}

	public void setReplys(Long replys) {
		this.replys = replys;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public void increaseLikes() {
		this.likes++;
	}

	public void increaseReplys() {
		this.replys++;
	}

}
package br.com.bexs.desafio_bexs.service;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.bexs.desafio_bexs.enums.AnsweredEnum;
import br.com.bexs.desafio_bexs.model.Question;
import br.com.bexs.desafio_bexs.model.repository.QuestionRepository;
import br.com.bexs.desafio_bexs.model.repository.specification.SearchCriteria;
import br.com.bexs.desafio_bexs.model.repository.specification.SearchOperation;
import br.com.bexs.desafio_bexs.model.repository.specification.SpecificationBuilder;
import br.com.bexs.desafio_bexs.model.searchable.QuestionSearchable;

@Service
public class QuestionService {

	private static final Logger log = LoggerFactory.getLogger(QuestionService.class);

	@Autowired
	private QuestionRepository repository;

	public Page<Question> findAll(QuestionSearchable search) {

		SpecificationBuilder<Question> builder = new SpecificationBuilder<Question>();

		if (search.getBean().getText() != null) {
			builder.with(new SearchCriteria("text", SearchOperation.ILIKE, search.getBean().getText()));
		}
		if (search.getAnswered() != null) {
			switch (AnsweredEnum.get(search.getAnswered())) {
			case NOT:
				builder.with(new SearchCriteria("replys", SearchOperation.EQUAL, 0));
				break;
			case YES:
				builder.with(new SearchCriteria("replys", SearchOperation.GREATER_THAN, 1));
				break;
			case ALL:
			default:
				break;
			}
		}

		if (search.getParams()) {
			builder.order(new SearchCriteria(search.getCollumn(), SearchOperation.ORDER_BY, search.getOrder()));
		}
		if (builder.hasCriteria()) {
			return repository.findAll(builder, search.getPageable());
		} else {
			return repository.findAll(search.getPageable());
		}
	}

	public Optional<Question> findById(Long id) {
		return repository.findById(id);
	}

	public Question save(Question obj) {
		obj.setCreationDate(new Date());
		return repository.save(obj);
	}

	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	public Question like(Long id) throws Exception {
		Optional<Question> bean = findById(id);
		if (!bean.isPresent()) {
			log.error("Question id: {} is not existed", id);
			throw new Exception();
		}
		bean.get().increaseLikes();
		return repository.save(bean.get());
	}

	public Question increaseReplys(Long id) throws Exception {
		Optional<Question> bean = findById(id);
		if (!bean.isPresent()) {
			log.error("Question id: {} is not existed", id);
			throw new Exception();
		}
		bean.get().increaseReplys();
		return repository.save(bean.get());
		
	}
}
package br.com.bexs.desafio_bexs.model.repository.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public final class SpecificationBuilder<T> implements Specification<T> {

	private static final long serialVersionUID = 1608612752431942730L;

	private final List<SearchCriteria> params;

	private final List<SearchCriteria> orders;

	private final List<String> groups;

	public SpecificationBuilder() {
		params = new ArrayList<>();
		groups = new ArrayList<>();
		orders = new ArrayList<>();
	}
	/**
	 * add a {@link SearchCriteria} to the builder.
	 * 
	 * @param criteria
	 * @return
	 */
	public final SpecificationBuilder<T> with(SearchCriteria criteria) {
		params.add(criteria);
		return this;
	}
	
	public final SpecificationBuilder<T> groupBy(String... description) {
		for (int i = 0; i < description.length; i++) {
			groups.add(description[i]);
		}
		return this;
	}
	
	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		List<Predicate> predicates = new ArrayList<>();

		// add add criteria to predicates
		for (SearchCriteria criteria : params) {
			Path<String> path = root.<String>get(criteria.getKey());
			switch (criteria.getOperation()) {
			case GREATER_THAN :
					predicates.add(builder.greaterThanOrEqualTo(path, criteria.getValue().toString()));
				break;
			case LESS_THAN :
				predicates.add(builder.lessThanOrEqualTo(path, criteria.getValue().toString()));
				break;
			case EQUAL:
			case LIKE:
				if (path.getJavaType() == String.class) {
					predicates.add(builder.like(path, "%" + criteria.getValue() + "%"));
				} else {
					predicates.add(builder.equal(path, criteria.getValue()));
				}
				break;
			case ILIKE:
					predicates.add(
						builder.or(
							builder.like(builder.lower(path),
									"%" + ((String) criteria.getValue()).toLowerCase() + "%"),
							builder.like(builder.lower(path),
									"%" + ((String) criteria.getValue()).toLowerCase() + "%"),
							builder.like(builder.lower(path),
									"%" + ((String) criteria.getValue()).toLowerCase() + "%"))
					);
				break;
			case ORDER_BY:
				if (criteria.getValue() != null && criteria.getKey() != null) {
					if ("asc".equalsIgnoreCase(criteria.getValue().toString())) {
						query.orderBy(builder.asc(path));
					} else if ("desc".equalsIgnoreCase(criteria.getValue().toString())) {
						query.orderBy(builder.desc(path));
					}
				}
				break;
			case IS_NULL:
				predicates.add(builder.isNull(path));
				break;
			case NOT_NULL:
				predicates.add(builder.isNotNull(path));
				break;
			case IN:
				predicates.add(builder.isTrue(path.in((List)criteria.getValue())));
				break;
			case LIMIT:
			case NULL:
			default:
				break;
			}
		}
			
			
		List<Expression<?>> expressions = new ArrayList<Expression<?>>();
		
		for (String attribute : groups) {
			expressions.add(query.groupBy(root.<String>get(attribute)).getGroupList().get(0));
		}
		if(!orders.isEmpty()) {
			List<Order> orderList = new ArrayList<Order>();
			for (SearchCriteria order : orders) {
				if(order.getValue().equals("asc")) {
					orderList.add(builder.asc(root.get(order.getKey())));
				}else {
					orderList.add(builder.desc(root.get(order.getKey())));
				}
			}
			query.orderBy(orderList);
		}
		query.groupBy(expressions);
		
		return builder.and(predicates.toArray(new Predicate[0]));
	}
	
	public boolean hasCriteria() {
		return !params.isEmpty();
	}
	
	public SpecificationBuilder<T> order(SearchCriteria criteria) {
		orders.add(criteria);
		return this;
	}

}
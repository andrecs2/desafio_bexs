package br.com.bexs.desafio_bexs.model.searchable;

import br.com.bexs.desafio_bexs.model.Question;

public class QuestionSearchable extends Searcheable<Question> {

	private String answered;

	public String getAnswered() {
		return this.answered;
	}

	public void setAnswered(String answered) {
		this.answered = answered;
	}


}

package br.com.bexs.desafio_bexs.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bexs.desafio_bexs.model.Question;
import br.com.bexs.desafio_bexs.model.searchable.QuestionSearchable;
import br.com.bexs.desafio_bexs.service.QuestionService;

@RestController
@RequestMapping("/api/v1/question")
public class QuestionController {
	
	@Autowired
	private QuestionService service;
	
	private static final Logger log = LoggerFactory.getLogger(QuestionController.class);
	
	@GetMapping
	public ResponseEntity<Page<Question>> findAll(QuestionSearchable search) {
		log.info("[GET /api/v1/question]");
		return ResponseEntity.ok(service.findAll(search));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Question> findById(@PathVariable Long id) {
		Optional<Question> obj = service.findById(id);
		if (!obj.isPresent()) {
			log.error("Question id: " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(obj.get());
	}

	@PostMapping
	public ResponseEntity<Question> create(@RequestBody Question bean) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(bean));
	}

	@PutMapping("/{id}")
	public ResponseEntity<Question> update(@PathVariable Long id, @RequestBody Question bean) {
		Optional<Question> obj = service.findById(id);
		if (!obj.isPresent()) {
			log.error("Question {} is not existed", id);
			ResponseEntity.badRequest().build();
		}
		return ResponseEntity.accepted().body(service.save(bean));
	}
	@GetMapping("/{id}/like")
	public ResponseEntity<Question> like(@PathVariable Long id) throws Exception {
		return ResponseEntity.accepted().body(service.like(id));
	}

}
package br.com.bexs.desafio_bexs.service;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.bexs.desafio_bexs.model.Answer;
import br.com.bexs.desafio_bexs.model.repository.AnswerRepository;
import br.com.bexs.desafio_bexs.model.repository.specification.SearchCriteria;
import br.com.bexs.desafio_bexs.model.repository.specification.SearchOperation;
import br.com.bexs.desafio_bexs.model.repository.specification.SpecificationBuilder;
import br.com.bexs.desafio_bexs.model.searchable.AnswerSearchable;

@Service
public class AnswerService {
	
	private static final Logger log = LoggerFactory.getLogger(AnswerService.class);


	@Autowired
	private AnswerRepository repository;
	
	@Autowired
	private QuestionService questionService;

	public Page<Answer> findAll(AnswerSearchable search) {

		SpecificationBuilder<Answer> builder = new SpecificationBuilder<Answer>();

		builder.with(new SearchCriteria("question", SearchOperation.EQUAL, search.getBean().getQuestion()));
		if (search.getBean().getText() != null) {
			builder.with(new SearchCriteria("text", SearchOperation.ILIKE, search.getBean().getText()));
		}
		

		if (search.getParams()) {
			builder.order(new SearchCriteria(search.getCollumn(), SearchOperation.ORDER_BY, search.getOrder()));
		}
		if (builder.hasCriteria()) {
			return repository.findAll(builder, search.getPageable());
		} else {
			return repository.findAll(search.getPageable());
		}
	}

	public Optional<Answer> findById(Long id) {
		return repository.findById(id);
	}

	@Transactional
	public Answer save(Answer obj) throws Exception {
		questionService.increaseReplys(obj.getQuestion().getId());
		obj.setCreationDate(new Date());
		return repository.save(obj);
	}


	public Answer like(Long id) throws Exception {
		Optional<Answer> bean = findById(id);
		if (!bean.isPresent()) {
			log.error("Question id: {} is not existed", id);
			throw new Exception();
		}
		bean.get().increaseLikes();
		return repository.save(bean.get());
	}
}
package br.com.bexs.desafio_bexs.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bexs.desafio_bexs.model.Answer;
import br.com.bexs.desafio_bexs.model.searchable.AnswerSearchable;
import br.com.bexs.desafio_bexs.service.AnswerService;

@RestController
@RequestMapping("/api/v1/answer")
public class AnswerController {
	
	@Autowired
	private AnswerService service;
	
	private static final Logger log = LoggerFactory.getLogger(AnswerController.class);
	
	@GetMapping
	public ResponseEntity<Page<Answer>> findAll(AnswerSearchable search) {
		log.info("[GET /api/v1/answer]");
		return ResponseEntity.ok(service.findAll(search));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Answer> findById(@PathVariable Long id) {
		Optional<Answer> bean = service.findById(id);
		if (!bean.isPresent()) {
			log.error("Answer Id: " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(bean.get());
	}

	@PostMapping
	public ResponseEntity<Answer> create(@RequestBody Answer bean) throws Exception {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(bean));
	}

	@PutMapping("/{id}")
	public ResponseEntity<Answer> update(@PathVariable Long id, @RequestBody Answer bean) throws Exception {
		Optional<Answer> obj = service.findById(id);
		if (!obj.isPresent()) {
			log.error("Question id: {} is not existed", id);
			ResponseEntity.badRequest().build();
		}
		return ResponseEntity.accepted().body(service.save(bean));
	}
	
	@GetMapping("/{id}/like")
	public ResponseEntity<Answer> like(@PathVariable Long id) throws Exception {
		log.info("[GET /api/v1/answer/{}]");
		return ResponseEntity.accepted().body(service.like(id));
	}

}
package br.com.bexs.desafio_bexs.model.repository.specification;


public enum SearchOperation {
	
	NULL
	,EQUAL
	,LIKE
	,LESS_THAN
	,GREATER_THAN
	,ORDER_BY
	,IS_NULL
	,NOT_NULL
	,LIMIT
	,IN
	,ILIKE;
}
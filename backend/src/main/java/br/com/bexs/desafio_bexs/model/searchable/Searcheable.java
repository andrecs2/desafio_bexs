package br.com.bexs.desafio_bexs.model.searchable;


import java.lang.reflect.ParameterizedType;

import org.springframework.data.domain.PageRequest;

public class Searcheable<T> {
	
	private int index; 

	private int size;

	private T bean;

	private String collumn;

	private String order;

	public T getBean() {
		if (bean == null) {
			ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
			Class<T> type = (Class<T>) superClass.getActualTypeArguments()[0];
			try {
				this.bean = type.newInstance();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return bean;
	}

	public void setBean(T bean) {
		this.bean = bean;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int pageSize) {
		this.size = pageSize;
	}


	public boolean getParams() {
		return this.order != null && !"".equals(this.order) && !"".equals(this.collumn);
	}

	public String getCollumn() {
		return collumn;
	}

	public String getOrder() {
		return order;
	}

	public void setCollumn(String collumn) {
		this.collumn = collumn;
	}

	public void setOrder(String order) {
		this.order = order;
	}
	
	public PageRequest getPageable() {
		if (this.size == 0 || this.size < 0) {
			this.size = Short.MAX_VALUE;
		}
		return PageRequest.of(this.index, this.size);
	}

}

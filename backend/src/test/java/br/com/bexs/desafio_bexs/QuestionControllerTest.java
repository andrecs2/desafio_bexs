package br.com.bexs.desafio_bexs;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.bexs.desafio_bexs.model.Question;
import br.com.bexs.desafio_bexs.model.repository.AnswerRepository;
import br.com.bexs.desafio_bexs.model.searchable.QuestionSearchable;
import br.com.bexs.desafio_bexs.service.QuestionService;

@RunWith(SpringRunner.class)
@WebMvcTest
public class QuestionControllerTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private QuestionService service;
	@MockBean
	private AnswerRepository answerRepository;

	@Test
	public void findAll() throws Exception {
		// Question bean = new Question();
		// bean.setId(1L);
		// bean.setText("Question 1");

		// List<Question> list = Arrays.asList(bean);
		// QuestionSearchable search = new QuestionSearchable();
		// search.setBean(bean);
		// given(service.findAll(search).getContent()).willReturn(list);

		// // when + then
		// this.mockMvc.perform(get("/api/v1/question")).andExpect(status().isOk())
		// 		.andExpect(content().json("[{'id': 1,'name': 'Question 1';'price': 1}]"));
	}	

}